build:
	docker-compose build

up:
	docker-compose up -d

down:
	docker-compose down

logs:
	docker logs loan_app -f

bash:
	docker exec -it -u node loan_app bash
