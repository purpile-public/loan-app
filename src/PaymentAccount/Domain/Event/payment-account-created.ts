import { MonetaryAmount } from '../../../Shared/Domain/ValueObject/monetary-amount';
import { Uuid } from '../../../Shared/Domain/ValueObject/uuid';
import { Event } from '../../../Shared/Domain/event';
import { AccountNumber } from "../ValueObject/account-number";

export class PaymentAccountCreated implements Event {
  constructor(
    public readonly id: Uuid,
    public readonly money: MonetaryAmount,
    public readonly customerId: Uuid,
    public readonly accountNumber: AccountNumber,
  ) {}
}
