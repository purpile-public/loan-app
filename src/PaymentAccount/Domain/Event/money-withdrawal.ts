import { MonetaryAmount } from '../../../Shared/Domain/ValueObject/monetary-amount';
import { Uuid } from '../../../Shared/Domain/ValueObject/uuid';
import { Event } from '../../../Shared/Domain/event';

export class MoneyWithdrawal implements Event {
  constructor(
    public readonly id: Uuid,
    public readonly money: MonetaryAmount,
  ) {}
}
