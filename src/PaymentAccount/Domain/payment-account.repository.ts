import { PaymentAccount } from './payment-account';
import { Uuid } from '../../Shared/Domain/ValueObject/uuid';

export interface PaymentAccountRepository {
  store(paymentAccount: PaymentAccount): void;

  findOneByCustomerId(customerId: Uuid): PaymentAccount;
}
