export class AccountNumber {
  private constructor(private _value: string) {}

  public static generate(): AccountNumber {
    return new AccountNumber(
      parseInt(String(Math.random() * 1000000000), 8).toString(),
    );
  }

  public static fromString(value: string): AccountNumber {
    return new AccountNumber(value);
  }

  public toString(): string {
    return this._value.toString();
  }
}
