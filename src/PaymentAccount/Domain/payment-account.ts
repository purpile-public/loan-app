import { AggregateModel } from '../../Shared/Domain/aggregate-model';
import { Uuid } from '../../Shared/Domain/ValueObject/uuid';
import { MonetaryAmount } from '../../Shared/Domain/ValueObject/monetary-amount';
import { MoneyWithdrawal } from './Event/money-withdrawal';
import { MoneyFunded } from './Event/money-funded';
import { PaymentAccountCreated } from './Event/payment-account-created';
import { AccountNumber } from './ValueObject/account-number';
import { Transaction } from './transaction';
import { DomainException } from '../../Shared/Domain/Exception/domain.exception';

export class PaymentAccount extends AggregateModel {
  private _id: Uuid;
  private _totalMoney: MonetaryAmount;
  private _customerId: Uuid;
  private _accountNumber: AccountNumber;
  private _transactions: Transaction[] = [];

  private constructor() {
    super();
  }

  public static create(
    id: Uuid,
    customerId: Uuid,
    accountNumber: AccountNumber,
  ): PaymentAccount {
    const object = new PaymentAccount();
    object._id = id;
    object._totalMoney = MonetaryAmount.default();
    object._customerId = customerId;
    object._accountNumber = accountNumber;

    object.recordEvent(
      new PaymentAccountCreated(
        id,
        object._totalMoney,
        customerId,
        accountNumber,
      ),
    );

    return object;
  }

  public charge(money: MonetaryAmount, loanId: Uuid, now: Date): void {
    if (this._totalMoney.lessThan(money)) {
      throw new DomainException('Account dont have enough money');
    }

    this._totalMoney = MonetaryAmount.fromInteger(
      this._totalMoney.toNumber() - money.toNumber(),
    );

    this._transactions.push(
      new Transaction(Uuid.generate(), money, loanId, now),
    );

    this.recordEvent(new MoneyWithdrawal(this._id, money));
  }

  public fund(money: MonetaryAmount): void {
    this._totalMoney = MonetaryAmount.fromInteger(
      this._totalMoney.toNumber() + money.toNumber(),
    );

    this.recordEvent(new MoneyFunded(this._id, money));
  }

  public toSnapshot(): object {
    return {
      id: this._id,
      totalMoney: this._totalMoney,
      customerId: this._customerId,
      accountNumber: this._accountNumber,
    };
  }
}
