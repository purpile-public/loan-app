import { Uuid } from '../../Shared/Domain/ValueObject/uuid';
import { MonetaryAmount } from '../../Shared/Domain/ValueObject/monetary-amount';

export class Transaction {
  public constructor(
    public readonly id: Uuid,
    public readonly amount: MonetaryAmount,
    public readonly loanId: Uuid,
    public readonly createdAt: Date,
  ) {}
}
