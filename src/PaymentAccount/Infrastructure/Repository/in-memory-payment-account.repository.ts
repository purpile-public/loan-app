import { Injectable } from '@nestjs/common';
import { PaymentAccount } from '../../Domain/payment-account';
import { PaymentAccountRepository } from '../../Domain/payment-account.repository';
import { Uuid } from '../../../Shared/Domain/ValueObject/uuid';

@Injectable()
export class InMemoryPaymentAccountRepository
  implements PaymentAccountRepository
{
  #stack: PaymentAccount[] = [];

  store(paymentAccount: PaymentAccount): void {
    this.#stack.push(paymentAccount);
  }

  findOneByCustomerId(customerId: Uuid): PaymentAccount {
    return this.#stack.pop();
  }
}
