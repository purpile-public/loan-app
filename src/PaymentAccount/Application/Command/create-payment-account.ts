export class CreatePaymentAccount {
  constructor(public readonly id: string, public readonly customerId: string) {}
}
