export class ChargePaymentAccount {
  constructor(
    public readonly customerId: string,
    public readonly amount: number,
    public readonly loanId: string,
    public readonly now: Date,
  ) {}
}
