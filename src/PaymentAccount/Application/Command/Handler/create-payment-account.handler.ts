import { CommandHandler, ICommandHandler } from '@nestjs/cqrs';
import { CreatePaymentAccount } from '../create-payment-account';
import { Uuid } from '../../../../Shared/Domain/ValueObject/uuid';
import { PaymentAccountRepository } from '../../../Domain/payment-account.repository';
import { PaymentAccount } from '../../../Domain/payment-account';
import { AccountNumber } from '../../../Domain/ValueObject/account-number';
import { Inject } from '@nestjs/common';

@CommandHandler(CreatePaymentAccount)
export class CreatePaymentAccountHandler
  implements ICommandHandler<CreatePaymentAccount>
{
  constructor(
    @Inject('PaymentAccountRepository')
    private readonly paymentAccountRepository: PaymentAccountRepository,
  ) {}

  async execute(command: CreatePaymentAccount) {
    const paymentAccount = PaymentAccount.create(
      Uuid.fromString(command.id),
      Uuid.fromString(command.customerId),
      AccountNumber.generate(),
    );

    this.paymentAccountRepository.store(paymentAccount);
  }
}
