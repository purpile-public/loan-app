import { CommandHandler, EventBus, ICommandHandler } from '@nestjs/cqrs';
import { Uuid } from '../../../../Shared/Domain/ValueObject/uuid';
import { PaymentAccountRepository } from '../../../Domain/payment-account.repository';
import { ChargePaymentAccount } from '../charge-payment-account';
import { MonetaryAmount } from '../../../../Shared/Domain/ValueObject/monetary-amount';
import { PaymentAccountCharged } from '../../Event/payment-account-charged';
import { Inject } from '@nestjs/common';

@CommandHandler(ChargePaymentAccount)
export class ChargePaymentAccountHandler
  implements ICommandHandler<ChargePaymentAccount>
{
  constructor(
    @Inject('PaymentAccountRepository')
    private readonly paymentAccountRepository: PaymentAccountRepository,
    private readonly eventBus: EventBus,
  ) {}

  async execute(command: ChargePaymentAccount) {
    const paymentAccount = this.paymentAccountRepository.findOneByCustomerId(
      Uuid.fromString(command.customerId),
    );

    paymentAccount.charge(
      MonetaryAmount.fromDecimal(command.amount),
      Uuid.fromString(command.loanId),
      command.now,
    );

    this.paymentAccountRepository.store(paymentAccount);

    this.eventBus.publish(
      // @ts-ignore - add interface later
      new PaymentAccountCharged(paymentAccount.toSnapshot().id, command.amount, command.loanId),
    );
  }
}
