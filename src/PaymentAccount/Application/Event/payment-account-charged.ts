import { Event } from '../../../Shared/Application/Event';

export class PaymentAccountCharged implements Event {
  constructor(
    public readonly id: string,
    public readonly amount: number,
    public readonly loanId: string,
  ) {}
}
