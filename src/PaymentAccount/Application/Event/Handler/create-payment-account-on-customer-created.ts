import { CommandBus, EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { CustomerCreated } from '../../../../Customer/Event/customer-created';
import { CreatePaymentAccount } from '../../Command/create-payment-account';
import { Uuid } from '../../../../Shared/Domain/ValueObject/uuid';

@EventsHandler(CustomerCreated)
export class CreatePaymentAccountOnCustomerCreated
  implements IEventHandler<CustomerCreated>
{
  constructor(private readonly commandBus: CommandBus) {}

  handle(event: CustomerCreated) {
    this.commandBus.execute(
      new CreatePaymentAccount(Uuid.generate().toString(), event.id),
    );
  }
}
