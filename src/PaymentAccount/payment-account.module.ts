import { Module } from '@nestjs/common';
import { CreatePaymentAccountOnCustomerCreated } from './Application/Event/Handler/create-payment-account-on-customer-created';
import { CqrsModule } from '@nestjs/cqrs';
import { InMemoryPaymentAccountRepository } from './Infrastructure/Repository/in-memory-payment-account.repository';
import { CreatePaymentAccountHandler } from './Application/Command/Handler/create-payment-account.handler';
import { ChargePaymentAccountHandler } from './Application/Command/Handler/charge-payment-account.handler';

export const EventHandlers = [CreatePaymentAccountOnCustomerCreated];
export const CommandHandlers = [
  CreatePaymentAccountHandler,
  ChargePaymentAccountHandler,
];

@Module({
  imports: [CqrsModule],
  providers: [
    ...EventHandlers,
    ...CommandHandlers,
    {
      provide: 'PaymentAccountRepository',
      useClass: InMemoryPaymentAccountRepository,
    },
  ],
})
export class PaymentAccountModule {}
