import { Module } from '@nestjs/common';
import { LoanModule } from './Loan/loan.module';
import { CustomerModule } from './Customer/customer.module';
import { PaymentAccountModule } from './PaymentAccount/payment-account.module';

@Module({
  imports: [LoanModule, CustomerModule, PaymentAccountModule],
  controllers: [],
  providers: [],
})
export class AppModule {}
