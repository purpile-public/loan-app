import { Module } from '@nestjs/common';
import { CqrsModule } from '@nestjs/cqrs';
import { InMemoryCustomerRepository } from './Repository/in-memory-customer.repository';
import { CustomerApi } from './Controller/customer-api';
import { CustomerService } from './Service/customer-service';
import { CustomerProvider } from './Provider/customer.provider';

@Module({
  imports: [CqrsModule],
  exports: [
    {
      provide: 'CustomerRepository',
      useClass: InMemoryCustomerRepository,
    },
  ],
  controllers: [CustomerApi],
  providers: [
    CustomerProvider,
    {
      provide: 'CustomerRepository',
      useClass: InMemoryCustomerRepository,
    },
    CustomerService,
  ],
})
export class CustomerModule {}
