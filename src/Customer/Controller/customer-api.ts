import { Response } from 'express';
import { Body, Controller, Get, Param, Post, Put, Res } from '@nestjs/common';
import { CreateCustomerDto } from './Dto/create-customer.dto';
import { CustomerService } from '../Service/customer-service';
import { CustomerProvider } from '../Provider/customer.provider';
@Controller('/api/customers')
export class CustomerApi {
  constructor(
    private readonly customerService: CustomerService,
    private readonly customerProvider: CustomerProvider,
  ) {}

  @Post()
  async create(
    @Body() body: CreateCustomerDto,
    @Res() res: Response,
  ): Promise<Response> {
    const customer = this.customerService.create(body);

    return res.status(201).send({ id: customer.id });
  }

  @Put('/:id')
  async update(
    @Param('id') id: string,
    @Body() body: CreateCustomerDto,
    @Res() res: Response,
  ): Promise<Response> {
    this.customerService.update(id, body);

    return res.status(204).send();
  }

  @Get('/:id')
  async get(@Param('id') id: string, @Res() res: Response): Promise<Response> {
    try {
      return res.status(200).send(this.customerProvider.findById(id));
    } catch (e) {
      return res.status(404);
    }
  }
}
