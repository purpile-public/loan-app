export class CustomerDetailsDto {
  constructor(
    public readonly id: string,
    public readonly firstName: string,
    public readonly lastName: string,
    public readonly age: number,
    public readonly fullName: string,
  ) {}
}
