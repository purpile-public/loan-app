import { CustomerRepository } from './customer.repository';
import { Customer } from '../Model/customer';
import { Injectable } from '@nestjs/common';

@Injectable()
export class InMemoryCustomerRepository implements CustomerRepository {
  #customers: Customer[] = [];

  store(customer: Customer): void {
    const recordIndex = this.#customers.findIndex(
      (e: Customer) => e.id === customer.id,
    );

    if (recordIndex !== -1) {
      return;
    }

    this.#customers.push(customer);
  }

  find(id: string): Customer | null {
    return this.#customers.find((value) => value.id === id) ?? null;
  }
}
