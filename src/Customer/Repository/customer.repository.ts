import { Customer } from '../Model/customer';

export interface CustomerRepository {
  store(customer: Customer): void;

  find(id: string): Customer | null;
}
