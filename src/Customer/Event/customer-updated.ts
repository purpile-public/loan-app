import { Event } from '../../Shared/Application/Event';

export class CustomerUpdated implements Event {
  constructor(public readonly id: string) {}
}
