import { Event } from '../../Shared/Application/Event';

export class CustomerCreated implements Event {
  constructor(public readonly id: string) {}
}
