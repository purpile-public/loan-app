export class Customer {
  constructor(
    private _id: string,
    private _firstName: string,
    private _lastName: string,
    private _age: number,
  ) {}

  get id(): string {
    return this._id;
  }

  get firstName(): string {
    return this._firstName;
  }

  get lastName(): string {
    return this._lastName;
  }

  get age(): number {
    return this._age;
  }

  set firstName(value: string) {
    this._firstName = value;
  }

  set lastName(value: string) {
    this._lastName = value;
  }
}
