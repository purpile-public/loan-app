import { CustomerDetailsDto } from '../Controller/Dto/Response/customer-details.dto';
import { Inject } from '@nestjs/common';
import { CustomerRepository } from '../Repository/customer.repository';

export class CustomerProvider {
  constructor(
    @Inject('CustomerRepository')
    private readonly repository: CustomerRepository,
  ) {}
  findById(id: string): CustomerDetailsDto {
    const customer = this.repository.find(id);

    if (null === customer) {
      throw new Error('Not found');
    }

    return new CustomerDetailsDto(
      customer.id,
      customer.firstName,
      customer.lastName,
      customer.age,
      `${customer.firstName} ${customer.lastName}`,
    );
  }
}
