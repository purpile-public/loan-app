import { Customer } from '../Model/customer';
import { CreateCustomerDto } from '../Controller/Dto/create-customer.dto';
import { CustomerRepository } from '../Repository/customer.repository';
import { Uuid } from '../../Shared/Domain/ValueObject/uuid';
import { EventBus } from '@nestjs/cqrs';
import { CustomerCreated } from '../Event/customer-created';
import { UpdateCustomerDto } from '../Controller/Dto/update-customer.dto';
import { CustomerUpdated } from '../Event/customer-updated';
import { Inject } from '@nestjs/common';

export class CustomerService {
  constructor(
    @Inject('CustomerRepository')
    private readonly repository: CustomerRepository,
    private readonly eventBus: EventBus,
  ) {}

  create(payload: CreateCustomerDto): Customer {
    const customer = new Customer(
      Uuid.generate().toString(),
      payload.firstName,
      payload.lastName,
      payload.age,
    );

    this.repository.store(customer);

    this.eventBus.publish(new CustomerCreated(customer.id.toString()));

    return customer;
  }

  update(id: string, payload: UpdateCustomerDto): Customer {
    const customer = this.repository.find(id);

    customer.firstName = payload.firstName;
    customer.lastName = payload.lastName;

    this.repository.store(customer);

    this.eventBus.publish(new CustomerUpdated(customer.id.toString()));

    return customer;
  }
}
