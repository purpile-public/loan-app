import { CustomerRepository } from '../../../Customer/Repository/customer.repository';
import { Inject } from '@nestjs/common';
import { CustomerProvider } from '../../Domain/Offer/Provider/customer-provider';
import { Uuid } from '../../../Shared/Domain/ValueObject/uuid';
import { Customer } from '../../Domain/Offer/Provider/customer';

export class ModuleCustomerProvider implements CustomerProvider {
  constructor(
    @Inject('CustomerRepository')
    private readonly repository: CustomerRepository,
  ) {}

  provide(customerId: Uuid): Customer {
    return this.repository.find(customerId.toString());
  }
}
