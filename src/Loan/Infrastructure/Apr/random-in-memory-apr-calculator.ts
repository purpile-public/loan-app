import { AprCalculator } from '../../Domain/Offer/apr.calculator';
import { Apr } from '../../Domain/Shared/apr';
import { Uuid } from '../../../Shared/Domain/ValueObject/uuid';
import { Injectable } from '@nestjs/common';

@Injectable()
export class RandomInMemoryAprCalculator implements AprCalculator {
  calculate(customerId: Uuid): Apr {
    return new Apr(Math.floor(Math.random() * 22) + 14);
  }
}
