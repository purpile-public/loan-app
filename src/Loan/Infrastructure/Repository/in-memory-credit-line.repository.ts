import { Injectable } from '@nestjs/common';
import { Uuid } from '../../../Shared/Domain/ValueObject/uuid';
import { CreditLineRepository } from '../../Domain/CreditLine/credit-line.repository';
import { CreditLine } from '../../Domain/CreditLine/credit-line';

@Injectable()
export class InMemoryCreditLineRepository implements CreditLineRepository {
  #stack: object[] = [];

  findOneByLoanId(loanId: Uuid): CreditLine {
    // @ts-ignore
    const creditLine = this.#stack.find((e) => e.loanId === loanId.toString());

    if (!creditLine) {
      throw new Error('Credit line not found');
    }

    return CreditLine.createFromHistory(creditLine['events']);
  }

  store(creditLine: CreditLine): void {
    // @ts-ignore
    const existedCreditLine = this.#stack.find((e) => e.id === creditLine.id);

    if (existedCreditLine) {
      // @ts-ignore
      existedCreditLine.events.push(creditLine.events);
      return;
    }

    this.#stack.push({
      id: creditLine.id.toString(),
      loanId: creditLine.loanId.toString(),
      events: creditLine.events,
    });
  }

  getAll(): object[] {
    return this.#stack;
  }
}
