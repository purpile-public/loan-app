import { Injectable } from '@nestjs/common';
import { LoanRepository } from '../../Domain/Loan/loan.repository';
import { Loan } from '../../Domain/Loan/loan';

@Injectable()
export class InMemoryLoanRepository implements LoanRepository {
  #stack: object[] = [];

  store(loan: Loan): void {
    // @ts-ignore
    const existedLoan = this.#stack.find((e) => e.id === loan.id);

    if (existedLoan) {
      // @ts-ignore
      existedLoan.events.push(loan.events);
      return;
    }

    this.#stack.push({
      id: loan.id,
      events: loan.events,
    });
  }

  getAll(): object[] {
    return this.#stack;
  }
}
