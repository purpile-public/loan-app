import { Injectable } from '@nestjs/common';
import { CreditLineRepository } from '../../Application/Projection/ReadModel/credit-line.repository';
import { CreditLine } from '../../Application/Projection/ReadModel/credit-line';

@Injectable()
export class InMemoryCreditLineReadModelRepository
  implements CreditLineRepository
{
  #stack: CreditLine[] = [];

  store(creditLine: CreditLine): void {
    const recordIndex = this.#stack.findIndex(
      (e: CreditLine) => e.creditLineId === creditLine.creditLineId,
    );

    if (recordIndex !== -1) {
      this.#stack[recordIndex] = creditLine;
      return;
    }

    this.#stack.push(creditLine);
  }

  find(creditLineId: string): CreditLine {
    return this.#stack.find((e: CreditLine) => e.creditLineId === creditLineId);
  }

  findAll(): CreditLine[] {
    return this.#stack;
  }
}
