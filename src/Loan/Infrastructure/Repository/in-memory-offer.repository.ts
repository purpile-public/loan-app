import { Injectable } from '@nestjs/common';
import { OfferRepository } from '../../Domain/Offer/offer.repository';
import { Offer } from '../../Domain/Offer/offer';
import { Uuid } from '../../../Shared/Domain/ValueObject/uuid';

@Injectable()
export class InMemoryOfferRepository implements OfferRepository {
  #stack: Offer[] = [];

  store(offer: Offer): void {
    const recordIndex = this.#stack.findIndex(
      (e: Offer) => e.id.toString() === offer.id.toString(),
    );

    if (recordIndex !== -1) {
      return;
    }

    this.#stack.push(offer);
  }

  find(id: Uuid): Offer {
    const offer = this.#stack.find(
      (value) => value.id.toString() === id.toString(),
    );

    if (undefined === offer) {
      throw new Error('Offer not found');
    }

    return offer;
  }
}
