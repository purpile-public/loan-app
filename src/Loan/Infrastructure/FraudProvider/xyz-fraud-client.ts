import { XyzFraudClientReport } from './xyz-fraud-client-report';

export class XyzFraudClient {
  getReport(firstName: string): XyzFraudClientReport {
    if (firstName === 'John') {
      return new XyzFraudClientReport(new Date());
    } else {
      return new XyzFraudClientReport();
    }
  }
}