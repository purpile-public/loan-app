import { FraudChecker } from '../../Domain/Offer/Matrix/Fraud/fraud-checker';
import { XyzFraudClient } from './xyz-fraud-client';
import { Uuid } from '../../../Shared/Domain/ValueObject/uuid';
import { CustomerProvider } from '../../Domain/Offer/Provider/customer-provider';
import { Inject } from '@nestjs/common';

export class XyzFraudChecker implements FraudChecker {
  constructor(
    private readonly client: XyzFraudClient,
    @Inject('CustomerProvider')
    private readonly customerProvider: CustomerProvider,
  ) {}

  check(customerId: Uuid): void {
    const report = this.client.getReport(
      this.customerProvider.provide(customerId).firstName,
    );

    if (null !== report.lastFraudNoticedDate) {
      throw new Error('Fraud!');
    }
  }
}
