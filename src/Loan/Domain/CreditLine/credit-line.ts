import { AggregateModel } from '../../../Shared/Domain/aggregate-model';
import { Uuid } from '../../../Shared/Domain/ValueObject/uuid';
import { MonetaryAmount } from '../../../Shared/Domain/ValueObject/monetary-amount';
import { CreditLineCreated } from './Event/credit-line-created';
import { Event } from '../../../Shared/Domain/event';
import { InstallmentNumber } from '../Shared/installment-number';
import { InstallmentPaid } from './Event/installment-paid';
import { DomainException } from '../../../Shared/Domain/Exception/domain.exception';

export class CreditLine extends AggregateModel {
  private _eventMap = {
    CreditLineCreated: 'whenCreditLineCreated',
    InstallmentPaid: 'whenInstallmentPaid',
  };

  private _id: Uuid;
  private _loanId: Uuid;
  private _totalPaidAmount: MonetaryAmount;
  private _totalCreditAmount: MonetaryAmount;
  private _numberOfInstallment: InstallmentNumber;
  private _totalNumberOfInstallment: InstallmentNumber;

  private constructor() {
    super();
  }

  get id(): Uuid {
    return this._id;
  }

  get loanId(): Uuid {
    return this._loanId;
  }

  protected recordEvent(event: Event) {
    super.recordEvent(event);
    this.applyEvent(event);
  }

  private applyEvent(event: Event): void {
    const method = this._eventMap[event.constructor.name];

    if (typeof this[method] !== 'function') {
      throw new DomainException(
        `Cannot apply event. Method does not found for event ${event.constructor.name}`,
      );
    }

    this[method](event);
  }

  public static create(
    id: Uuid,
    totalCreditAmount: MonetaryAmount,
    numberOfInstallment: InstallmentNumber,
    loanId: Uuid,
  ): CreditLine {
    const creditLine = new CreditLine();

    creditLine.recordEvent(
      new CreditLineCreated(id, totalCreditAmount, numberOfInstallment, loanId),
    );

    return creditLine;
  }

  public static createFromHistory(events: Array<Event>): CreditLine {
    const creditLine = new CreditLine();

    events.forEach((e) => {
      creditLine.applyEvent(e);
    });

    return creditLine;
  }

  public payInstallment(priceAmount: MonetaryAmount): void {
    if (this.calculateInstallmentUnit() !== priceAmount.toNumber()) {
      throw new DomainException('Price amount is not equals');
    }

    if (
      this._totalCreditAmount.toNumber() <
      Math.abs(priceAmount.toNumber() + this._totalPaidAmount.toNumber())
    ) {
      throw new DomainException('Cannot paid more then total');
    }

    this.recordEvent(new InstallmentPaid(this.id, priceAmount));
  }

  private calculateInstallmentUnit(): number {
    return Math.abs(
      this._totalCreditAmount.toNumber() / this._totalNumberOfInstallment.value,
    );
  }

  private whenCreditLineCreated(event: CreditLineCreated): void {
    this._id = event.id;
    this._loanId = event.loanId;
    this._totalCreditAmount = event.totalCreditAmount;
    this._totalPaidAmount = MonetaryAmount.default();
    this._numberOfInstallment = new InstallmentNumber(0);
    this._totalNumberOfInstallment = event.numberOfInstallment;
  }

  private whenInstallmentPaid(event: InstallmentPaid): void {
    this._totalPaidAmount = MonetaryAmount.fromInteger(
      this._totalPaidAmount.toNumber() + event.amount.toNumber(),
    );
    this._numberOfInstallment = new InstallmentNumber(
      Math.abs(this._numberOfInstallment.value + 1),
    );
  }
}
