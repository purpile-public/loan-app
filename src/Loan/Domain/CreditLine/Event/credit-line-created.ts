import { Event } from '../../../../Shared/Domain/event';
import { MonetaryAmount } from '../../../../Shared/Domain/ValueObject/monetary-amount';
import { Uuid } from '../../../../Shared/Domain/ValueObject/uuid';
import { InstallmentNumber } from '../../Shared/installment-number';

export class CreditLineCreated implements Event {
  constructor(
    public readonly id: Uuid,
    public readonly totalCreditAmount: MonetaryAmount,
    public readonly numberOfInstallment: InstallmentNumber,
    public readonly loanId: Uuid,
  ) {}
}
