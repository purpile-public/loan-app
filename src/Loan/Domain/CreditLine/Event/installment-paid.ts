import { Event } from '../../../../Shared/Domain/event';
import { MonetaryAmount } from '../../../../Shared/Domain/ValueObject/monetary-amount';
import { Uuid } from '../../../../Shared/Domain/ValueObject/uuid';

export class InstallmentPaid implements Event {
  constructor(
    public readonly id: Uuid,
    public readonly amount: MonetaryAmount,
  ) {}
}
