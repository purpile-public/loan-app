export enum LoanType {
  CONSUMER_CREDIT = 'consumer_credit',
  BUSINESS_CREDIT = 'business_credit',
}
