import { CreditLine } from "./credit-line";
import { Uuid } from "../../../Shared/Domain/ValueObject/uuid";

export interface CreditLineRepository {
  store(creditLine: CreditLine): void;

  findOneByLoanId(loanId: Uuid): CreditLine;
}
