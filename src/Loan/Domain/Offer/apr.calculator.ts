import { Apr } from '../Shared/apr';
import { Uuid } from '../../../Shared/Domain/ValueObject/uuid';

export interface AprCalculator {
  calculate(customerId: Uuid): Apr;
}
