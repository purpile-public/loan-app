export interface OfferSnapshot {
  id: string;
  customerId: string;
  apr: number;
  offerState: string;
  offerType: string;
  amount: number;
  numberOfInstallments: number;
}
