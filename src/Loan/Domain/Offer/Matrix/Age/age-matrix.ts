import { Matrix } from '../matrix';
import { Uuid } from '../../../../../Shared/Domain/ValueObject/uuid';
import { CustomerProvider } from '../../Provider/customer-provider';
import { Inject, Injectable } from '@nestjs/common';

@Injectable()
export class AgeMatrix implements Matrix {
  constructor(@Inject('CustomerProvider') private provider: CustomerProvider) {}

  check(customerId: Uuid): void {
    const customer = this.provider.provide(customerId);

    if (17 >= customer.age) {
      throw new Error('AgeMatrix broken');
    }
  }
}
