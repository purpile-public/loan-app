import { Matrix } from '../matrix';
import { Uuid } from '../../../../../Shared/Domain/ValueObject/uuid';
import { FraudChecker } from './fraud-checker';
import { Inject, Injectable } from '@nestjs/common';

@Injectable()
export class FraudMatrix implements Matrix {
  constructor(@Inject('FraudChecker') private readonly checker: FraudChecker) {}

  check(customerId: Uuid): void {
    this.checker.check(customerId);
  }
}
