import { Uuid } from '../../../../../Shared/Domain/ValueObject/uuid';

export interface FraudChecker {
  check(customerId: Uuid): void;
}
