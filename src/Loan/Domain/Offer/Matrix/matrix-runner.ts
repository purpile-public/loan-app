import { Matrix } from './matrix';
import { Uuid } from '../../../../Shared/Domain/ValueObject/uuid';
import { Injectable } from '@nestjs/common';

@Injectable()
export class MatrixRunner {
  private matrices: Matrix[] = [];

  public registerMatrices(matrices: Matrix[]): void {
    this.matrices = matrices;
  }

  run(customerId: Uuid): void {
    this.matrices.forEach((value) => value.check(customerId));
  }
}
