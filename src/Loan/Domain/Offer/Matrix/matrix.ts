import { Uuid } from '../../../../Shared/Domain/ValueObject/uuid';

export interface Matrix {
  check(customerId: Uuid): void;
}
