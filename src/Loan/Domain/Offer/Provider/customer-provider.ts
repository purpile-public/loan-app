import { Customer } from './customer';
import { Uuid } from '../../../../Shared/Domain/ValueObject/uuid';

export interface CustomerProvider {
  provide(customerId: Uuid): Customer;
}
