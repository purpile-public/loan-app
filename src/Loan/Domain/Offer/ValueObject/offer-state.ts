export enum OfferState {
  IN_PREPARING = 'in_preparing',
  PENDING = 'pending',
  ACCEPTED = 'accepted',
  REFUSED = 'refused',
  DECLINED_BY_MATRIX = 'declined_by_matrix',
}
