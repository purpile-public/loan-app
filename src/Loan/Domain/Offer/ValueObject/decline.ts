export class Decline {
  constructor(private value: string) {
    if (value.length > 100) {
      throw new Error('Invalid value');
    }
  }
}
