import { AggregateModel } from '../../../Shared/Domain/aggregate-model';
import { Uuid } from '../../../Shared/Domain/ValueObject/uuid';
import { Apr } from '../Shared/apr';
import { OfferState } from './ValueObject/offer-state';
import { OfferType } from './ValueObject/offer-type';
import { AprCalculator } from './apr.calculator';
import { OfferCreated } from './Event/offer-created';
import { AprCalculated } from './Event/apr-calculated';
import { OfferAccepted } from './Event/offer-accepted';
import { MatrixRunner } from './Matrix/matrix-runner';
import { Decline } from './ValueObject/decline';
import { MatrixSuccessfulFinished } from './Event/matrix-successful-finished';
import { MonetaryAmount } from '../../../Shared/Domain/ValueObject/monetary-amount';
import { InstallmentNumber } from '../Shared/installment-number';
import { OfferSnapshot } from './offer.snapshot';
import { MatrixUnsuccessfulFinished } from './Event/matrix-unsuccessful-finished';

export class Offer extends AggregateModel {
  private _id: Uuid;
  private _customerId: Uuid;
  private _apr: Apr;
  private _offerState: OfferState;
  private _offerType: OfferType;
  private _amount: MonetaryAmount;
  private _numberOfInstallments: InstallmentNumber;
  private _decline: Decline[] = [];

  private constructor() {
    super();
  }

  get id(): Uuid {
    return this._id;
  }

  static createOffer(
    id: Uuid,
    customerId: Uuid,
    amount: MonetaryAmount,
    numberOfInstallments: InstallmentNumber,
  ): Offer {
    const offer = new Offer();
    offer._offerState = OfferState.IN_PREPARING;
    offer._id = id;
    offer._customerId = customerId;
    offer._amount = amount;
    offer._numberOfInstallments = numberOfInstallments;
    offer._offerType = OfferType.BUSINESS; // Dummy-static implementation. Something like "resolver" should resolve by customerId what kind of type is it.

    offer.recordEvent(new OfferCreated(offer._id, offer._customerId));

    return offer;
  }

  runMatrices(matrixRunner: MatrixRunner): void {
    try {
      matrixRunner.run(this._customerId);

      this.recordEvent(
        new MatrixSuccessfulFinished(this._id, this._customerId),
      );
    } catch (e) {
      this._offerState = OfferState.DECLINED_BY_MATRIX;
      this._decline.push(new Decline(e.message));

      this.recordEvent(
        new MatrixUnsuccessfulFinished(this._id, this._customerId),
      );
    }
  }

  calculateApr(calculator: AprCalculator): void {
    this._apr = calculator.calculate(this._customerId);
    this._offerState = OfferState.PENDING;

    this.recordEvent(new AprCalculated(this._id, this._apr));
  }

  accept(): void {
    if (OfferState.PENDING !== this._offerState) {
      throw new Error('Cannot accept an offer');
    }

    this._offerState = OfferState.ACCEPTED;

    this.recordEvent(
      new OfferAccepted(
        this._id,
        this._customerId,
        this._apr,
        this._amount,
        this._offerType,
        this._numberOfInstallments,
      ),
    );
  }

  toSnapshot(): OfferSnapshot {
    return {
      id: this.id.toString(),
      customerId: this._customerId.toString(),
      offerType: this._offerType.toString(),
      amount: this._amount.toNumber(),
      offerState: this._offerState.toString(),
      numberOfInstallments: this._numberOfInstallments.value,
      apr: this._apr.value,
    };
  }
}
