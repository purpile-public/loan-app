import { Event } from '../../../../Shared/Domain/event';
import { Uuid } from '../../../../Shared/Domain/ValueObject/uuid';

export class OfferCreated implements Event {
  constructor(public readonly id: Uuid, public readonly customerId: Uuid) {}
}
