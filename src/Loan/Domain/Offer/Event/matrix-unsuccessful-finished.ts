import { Event } from '../../../../Shared/Domain/event';
import { Uuid } from '../../../../Shared/Domain/ValueObject/uuid';

export class MatrixUnsuccessfulFinished implements Event {
  constructor(public readonly id: Uuid, public readonly customerId: Uuid) {}
}
