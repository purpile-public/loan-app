import { Event } from '../../../../Shared/Domain/event';
import { Uuid } from '../../../../Shared/Domain/ValueObject/uuid';
import { Apr } from '../../Shared/apr';

export class AprCalculated implements Event {
  constructor(public readonly id: Uuid, public readonly apr: Apr) {}
}
