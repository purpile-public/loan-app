import { Event } from '../../../../Shared/Domain/event';
import { Uuid } from '../../../../Shared/Domain/ValueObject/uuid';
import { Apr } from '../../Shared/apr';
import { OfferType } from '../ValueObject/offer-type';
import { MonetaryAmount } from '../../../../Shared/Domain/ValueObject/monetary-amount';
import { InstallmentNumber } from '../../Shared/installment-number';

export class OfferAccepted implements Event {
  constructor(
    public readonly id: Uuid,
    public readonly customerId: Uuid,
    public readonly apr: Apr,
    public readonly amount: MonetaryAmount,
    public readonly offerType: OfferType,
    public readonly numberOfInstallments: InstallmentNumber,
  ) {}
}
