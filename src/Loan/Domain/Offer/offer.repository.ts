import { Offer } from './offer';
import { Uuid } from '../../../Shared/Domain/ValueObject/uuid';

export interface OfferRepository {
  store(offer: Offer): void;

  find(id: Uuid): Offer;
}
