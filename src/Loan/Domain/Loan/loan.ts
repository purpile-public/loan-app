import { AggregateModel } from '../../../Shared/Domain/aggregate-model';
import { Uuid } from '../../../Shared/Domain/ValueObject/uuid';
import { LoanCreated } from './Event/loan-created';
import { LoanType } from './ValueObject/loan-type';
import { Event } from '../../../Shared/Domain/event';
import { Apr } from '../Shared/apr';
import { MonetaryAmount } from '../../../Shared/Domain/ValueObject/monetary-amount';
import { InstallmentNumber } from '../Shared/installment-number';
import { LoanFinished } from './Event/loan-finished';
import { DomainException } from '../../../Shared/Domain/Exception/domain.exception';
import { AprCalculator } from '../Offer/apr.calculator';
import { LoanState } from './ValueObject/loan-state';

export class Loan extends AggregateModel {
  private _eventMap = {
    LoanCreated: 'whenLoanCreated',
    LoanFinished: 'whenLoanFinished',
  };
  private _id: Uuid;
  private _amount: MonetaryAmount;
  private _loanType: LoanType;
  private _customerId: Uuid;
  private _apr: Apr;
  private _loanState: LoanState;
  private _finishedAt?: Date;

  private constructor() {
    super();
  }

  get id(): Uuid {
    return this._id;
  }

  protected recordEvent(event: Event) {
    super.recordEvent(event);
    this.applyEvent(event);
  }

  private applyEvent(event: Event): void {
    const method = this._eventMap[event.constructor.name];

    if (typeof this[method] !== 'function') {
      throw new DomainException(
        `Cannot apply event. Method does not found for event ${event.constructor.name}`,
      );
    }

    this[method](event);
  }

  public static createBusinessLoan(
    id: Uuid,
    amount: MonetaryAmount,
    customerId: Uuid,
    apr: Apr,
    numberOfInstallment: InstallmentNumber,
  ): Loan {
    const loan = new Loan();

    loan.recordEvent(
      new LoanCreated(
        id,
        amount,
        LoanType.BUSINESS_CREDIT,
        customerId,
        apr,
        numberOfInstallment,
      ),
    );

    return loan;
  }

  public finished(now: Date): void {
    this.recordEvent(new LoanFinished(this.id, now));
  }

  public static createConsumerLoan(
    id: Uuid,
    amount: MonetaryAmount,
    customerId: Uuid,
    apr: Apr,
    numberOfInstallment: InstallmentNumber,
  ): Loan {
    const loan = new Loan();

    loan.recordEvent(
      new LoanCreated(
        id,
        amount,
        LoanType.CONSUMER_CREDIT,
        customerId,
        apr,
        numberOfInstallment,
      ),
    );

    return loan;
  }

  private whenLoanCreated(event: LoanCreated): void {
    this._id = event.id;
    this._customerId = event.customerId;
    this._amount = event.amount;
    this._loanType = event.type;
    this._apr = event.apr;
    this._loanState = LoanState.PENDING;
  }

  private whenLoanFinished(event: LoanFinished): void {
    this._loanState = LoanState.FINISHED;
    this._finishedAt = event.finishedAt;
  }
}
