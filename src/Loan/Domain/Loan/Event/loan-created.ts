import { Event } from '../../../../Shared/Domain/event';
import { Uuid } from '../../../../Shared/Domain/ValueObject/uuid';
import { LoanType } from '../ValueObject/loan-type';
import { Apr } from '../../Shared/apr';
import { InstallmentNumber } from '../../Shared/installment-number';
import { MonetaryAmount } from '../../../../Shared/Domain/ValueObject/monetary-amount';

export class LoanCreated implements Event {
  constructor(
    public readonly id: Uuid,
    public readonly amount: MonetaryAmount,
    public readonly type: LoanType,
    public readonly customerId: Uuid,
    public readonly apr: Apr,
    public readonly numberOfInstallment: InstallmentNumber,
  ) {}
}
