import { Event } from '../../../../Shared/Domain/event';
import { Uuid } from '../../../../Shared/Domain/ValueObject/uuid';

export class LoanFinished implements Event {
  constructor(public readonly id: Uuid, public readonly finishedAt: Date) {}
}
