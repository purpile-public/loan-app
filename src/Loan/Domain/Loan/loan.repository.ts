import { Loan } from './loan';

export interface LoanRepository {
  store(loan: Loan): void;
}
