export enum LoanState {
  PENDING = 'pending',
  FINISHED = 'finished',
}
