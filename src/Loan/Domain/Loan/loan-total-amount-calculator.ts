import { Apr } from '../Shared/apr';
import { MonetaryAmount } from '../../../Shared/Domain/ValueObject/monetary-amount';

export class LoanTotalAmountCalculator {
  static calculate(apr: Apr, totalAmount: MonetaryAmount): MonetaryAmount {
    return MonetaryAmount.fromInteger(
      Math.abs(
        (apr.value * totalAmount.toNumber()) / 100 + totalAmount.toNumber(),
      ),
    );
  }
}
