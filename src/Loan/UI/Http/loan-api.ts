import { Response } from 'express';
import {
  Body,
  Controller,
  Get,
  Inject,
  Param,
  Post,
  Req,
  Res,
} from '@nestjs/common';
import { CommandBus } from '@nestjs/cqrs';
import { Uuid } from '../../../Shared/Domain/ValueObject/uuid';
import { ChargePaymentAccount } from '../../../PaymentAccount/Application/Command/charge-payment-account';
import { ChargePayment } from './Dto/Request/charge-payment';
import { CreateOffer } from '../../Application/Command/create-offer';
import { AcceptOffer } from '../../Application/Command/accept-offer';
import { Quote } from './Dto/Request/quote';
import { CreditLineRepository } from '../../Application/Projection/ReadModel/credit-line.repository';

@Controller('/api/loans')
export class LoanApi {
  constructor(
    @Inject('ReadModelCreditLineRepository')
    private readonly creditLineRepository: CreditLineRepository,
    private readonly commandBus: CommandBus,
  ) {}

  @Post('/quote')
  createQuote(@Body() payload: Quote, @Res() res): Response {
    const id = Uuid.generate();

    this.commandBus.execute(
      new CreateOffer(
        id.toString(),
        payload.amount,
        payload.customerId,
        payload.numberOfInstallments,
      ),
    );

    return res.status(201).send({ id: id.toString() });
  }

  @Post('/offer/:id/accept')
  async acceptOffer(
    @Req() req,
    @Param('id') id: string,
    @Res() res,
  ): Promise<Response> {
    try {
      await this.commandBus.execute(new AcceptOffer(id));
    } catch (e) {
      return res.status(400).send({ message: e.message });
    }

    return res.status(204).send();
  }

  @Post('/:id/charge-payment')
  chargePaymentForLoan(
    @Body() payload: ChargePayment,
    @Param('id') id: string,
    @Res() res,
  ): Response {
    this.commandBus.execute(
      new ChargePaymentAccount(
        Uuid.generate().toString(),
        payload.amount,
        id,
        new Date(),
      ),
    );

    return res.status(204).send();
  }

  @Get('/:id/installment')
  getInstallment(@Param('id') id: string, @Res() res): Response {
    return res.status(200).send(this.creditLineRepository.find(id));
  }

  @Get('/installments')
  getInstallments(@Res() res): Response {
    return res.status(200).send(this.creditLineRepository.findAll());
  }
}
