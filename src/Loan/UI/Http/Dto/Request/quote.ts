import { IsNotEmpty, IsPositive, IsUUID } from 'class-validator';

export class Quote {
  @IsNotEmpty()
  @IsPositive()
  amount: number;
  @IsUUID()
  customerId: string;
  @IsPositive()
  @IsNotEmpty()
  numberOfInstallments: number;
}
