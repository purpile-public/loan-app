import { Injectable } from '@nestjs/common';
import { ICommand, ofType, Saga } from '@nestjs/cqrs';
import { map, Observable } from 'rxjs';
import { LoanCreated } from '../../Domain/Loan/Event/loan-created';
import { CreateCreditLine } from '../Command/create-credit-line';
import { Uuid } from '../../../Shared/Domain/ValueObject/uuid';
import { LoanTotalAmountCalculator } from '../../Domain/Loan/loan-total-amount-calculator';

@Injectable()
export class LoanCreatedManager {
  @Saga()
  process = (events$: Observable<any>): Observable<ICommand> => {
    return events$.pipe(
      ofType(LoanCreated),
      map(
        (event) =>
          new CreateCreditLine(
            Uuid.generate().toString(),
            event.id.toString(),
            LoanTotalAmountCalculator.calculate(
              event.apr,
              event.amount,
            ).toNumber(),
            event.numberOfInstallment.value,
          ),
      ),
    );
  };
}
