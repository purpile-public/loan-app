import { Injectable } from '@nestjs/common';
import { ICommand, ofType, Saga } from '@nestjs/cqrs';
import { map, Observable } from 'rxjs';
import { OfferCreated } from '../../Domain/Offer/Event/offer-created';
import { RunMatrix } from '../Command/run-matrix';

@Injectable()
export class OfferCreatedManager {
  @Saga()
  process = (events$: Observable<any>): Observable<ICommand> => {
    return events$.pipe(
      ofType(OfferCreated),
      map((event) => new RunMatrix(event.id.toString())),
    );
  };
}
