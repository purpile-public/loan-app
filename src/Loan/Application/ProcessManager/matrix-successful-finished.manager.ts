import { Injectable } from '@nestjs/common';
import { ICommand, ofType, Saga } from '@nestjs/cqrs';
import { map, Observable } from 'rxjs';
import { MatrixSuccessfulFinished } from '../../Domain/Offer/Event/matrix-successful-finished';
import { CalculateApr } from '../Command/calculate-apr';

@Injectable()
export class MatrixSuccessfulFinishedManager {
  @Saga()
  process = (events$: Observable<any>): Observable<ICommand> => {
    return events$.pipe(
      ofType(MatrixSuccessfulFinished),
      map((event) => new CalculateApr(event.id.toString())),
    );
  };
}
