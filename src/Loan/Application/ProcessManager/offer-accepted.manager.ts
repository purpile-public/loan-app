import { Injectable } from '@nestjs/common';
import { ICommand, ofType, Saga } from '@nestjs/cqrs';
import { map, Observable } from 'rxjs';
import { OfferAccepted } from '../../Domain/Offer/Event/offer-accepted';
import { CreateLoan } from '../Command/create-loan';
import { Uuid } from '../../../Shared/Domain/ValueObject/uuid';
import { OfferType } from '../../Domain/Offer/ValueObject/offer-type';
import { LoanType } from '../../Domain/Loan/ValueObject/loan-type';

@Injectable()
export class OfferAcceptedManager {
  @Saga()
  process = (events$: Observable<any>): Observable<ICommand> => {
    return events$.pipe(
      ofType(OfferAccepted),
      map(
        (event) =>
          new CreateLoan(
            Uuid.generate().toString(),
            event.customerId.toString(),
            event.amount.toDecimal(),
            event.offerType === OfferType.BUSINESS
              ? LoanType.BUSINESS_CREDIT.toString()
              : LoanType.CONSUMER_CREDIT.toString(),
            event.apr.value,
            event.numberOfInstallments.value,
          ),
      ),
    );
  };
}
