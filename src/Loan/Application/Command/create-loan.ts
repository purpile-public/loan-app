export class CreateLoan {
  constructor(
    public readonly id: string,
    public readonly customerId: string,
    public readonly totalAmount: number,
    public readonly type: string,
    public readonly apr: number,
    public readonly numberOfInstallments: number,
  ) {}
}
