export class CreateCreditLine {
  constructor(
    public readonly id: string,
    public readonly loanId: string,
    public readonly totalCreditAmount: number,
    public readonly totalNumberOfInstallment: number,
  ) {}
}
