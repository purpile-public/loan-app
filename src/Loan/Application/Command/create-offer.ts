export class CreateOffer {
  constructor(
    public readonly id: string,
    public readonly amount: number,
    public readonly customerId: string,
    public readonly installmentsNumber: number,
  ) {}
}
