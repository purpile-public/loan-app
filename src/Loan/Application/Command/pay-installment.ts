export class PayInstallment {
  constructor(public readonly loanId: string, public readonly amount: number) {}
}
