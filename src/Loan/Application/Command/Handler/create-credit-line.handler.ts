import { CommandHandler, EventBus, ICommandHandler } from '@nestjs/cqrs';
import { Uuid } from '../../../../Shared/Domain/ValueObject/uuid';
import { InstallmentNumber } from '../../../Domain/Shared/installment-number';
import { MonetaryAmount } from '../../../../Shared/Domain/ValueObject/monetary-amount';
import { CreateCreditLine } from '../create-credit-line';
import { CreditLine } from '../../../Domain/CreditLine/credit-line';
import { CreditLineRepository } from '../../../Domain/CreditLine/credit-line.repository';
import { Inject } from '@nestjs/common';

@CommandHandler(CreateCreditLine)
export class CreateCreditLineHandler
  implements ICommandHandler<CreateCreditLine>
{
  constructor(
    @Inject('CreditLineRepository')
    private readonly creditLineRepository: CreditLineRepository,
    private readonly eventBus: EventBus,
  ) {}

  async execute(command: CreateCreditLine) {
    const creditLine = CreditLine.create(
      Uuid.fromString(command.id),
      MonetaryAmount.fromInteger(command.totalCreditAmount),
      new InstallmentNumber(command.totalNumberOfInstallment),
      Uuid.fromString(command.loanId),
    );

    this.creditLineRepository.store(creditLine);

    this.eventBus.publish(creditLine.events.pop());
  }
}
