import { CommandHandler, EventBus, IEventHandler } from '@nestjs/cqrs';
import { Uuid } from '../../../../Shared/Domain/ValueObject/uuid';
import { CreditLineRepository } from '../../../Domain/CreditLine/credit-line.repository';
import { MonetaryAmount } from '../../../../Shared/Domain/ValueObject/monetary-amount';
import { Inject } from '@nestjs/common';
import { PayInstallment } from '../pay-installment';

@CommandHandler(PayInstallment)
export class PayInstallmentHandler implements IEventHandler<PayInstallment> {
  constructor(
    @Inject('CreditLineRepository')
    private readonly creditLineRepository: CreditLineRepository,
    private readonly eventBus: EventBus,
  ) {}

  handle(event: PayInstallment) {
    const creditLine = this.creditLineRepository.findOneByLoanId(
      Uuid.fromString(event.loanId),
    );

    creditLine.payInstallment(MonetaryAmount.fromDecimal(event.amount));

    this.creditLineRepository.store(creditLine);

    this.eventBus.publish(creditLine.events.pop());
  }
}
