import { CommandHandler, EventBus, ICommandHandler } from '@nestjs/cqrs';
import { Uuid } from '../../../../Shared/Domain/ValueObject/uuid';
import { Inject } from '@nestjs/common';
import { OfferRepository } from '../../../Domain/Offer/offer.repository';
import { RunMatrix } from '../run-matrix';
import { MatrixRunner } from '../../../Domain/Offer/Matrix/matrix-runner';

@CommandHandler(RunMatrix)
export class RunMatrixHandler implements ICommandHandler<RunMatrix> {
  constructor(
    @Inject('OfferRepository')
    private readonly offerRepository: OfferRepository,
    private eventBus: EventBus,
    @Inject('MATRIX')
    private matrixRunner: MatrixRunner,
  ) {}

  async execute(command: RunMatrix) {
    const offer = this.offerRepository.find(Uuid.fromString(command.id));
    offer.runMatrices(this.matrixRunner);

    this.offerRepository.store(offer);

    this.eventBus.publish(offer.events.pop());
  }
}
