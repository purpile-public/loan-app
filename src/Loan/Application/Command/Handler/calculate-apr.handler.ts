import { CommandHandler, EventBus, ICommandHandler } from '@nestjs/cqrs';
import { Uuid } from '../../../../Shared/Domain/ValueObject/uuid';
import { Inject } from '@nestjs/common';
import { OfferRepository } from '../../../Domain/Offer/offer.repository';
import { CalculateApr } from '../calculate-apr';
import { AprCalculator } from '../../../Domain/Offer/apr.calculator';

@CommandHandler(CalculateApr)
export class CalculateAprHandler implements ICommandHandler<CalculateApr> {
  constructor(
    @Inject('OfferRepository')
    private readonly offerRepository: OfferRepository,
    @Inject('AprCalculator')
    private readonly calculator: AprCalculator,
    private eventBus: EventBus,
  ) {}

  async execute(command: CalculateApr) {
    const offer = this.offerRepository.find(Uuid.fromString(command.id));
    offer.calculateApr(this.calculator);

    this.offerRepository.store(offer);

    this.eventBus.publish(offer.events.pop());
  }
}
