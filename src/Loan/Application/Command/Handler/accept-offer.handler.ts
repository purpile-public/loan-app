import { CommandHandler, EventBus, ICommandHandler } from '@nestjs/cqrs';
import { Uuid } from '../../../../Shared/Domain/ValueObject/uuid';
import { Inject } from '@nestjs/common';
import { OfferRepository } from '../../../Domain/Offer/offer.repository';
import { AcceptOffer } from '../accept-offer';

@CommandHandler(AcceptOffer)
export class AcceptOfferHandler implements ICommandHandler<AcceptOffer> {
  constructor(
    @Inject('OfferRepository')
    private readonly offerRepository: OfferRepository,
    private eventBus: EventBus,
  ) {}

  async execute(command: AcceptOffer) {
    const offer = this.offerRepository.find(Uuid.fromString(command.id));
    offer.accept();

    this.offerRepository.store(offer);

    this.eventBus.publish(offer.events.pop());
  }
}
