import { CommandHandler, EventBus, ICommandHandler } from '@nestjs/cqrs';
import { Uuid } from '../../../../Shared/Domain/ValueObject/uuid';
import { Inject } from '@nestjs/common';
import { CreateOffer } from '../create-offer';
import { Offer } from '../../../Domain/Offer/offer';
import { OfferRepository } from '../../../Domain/Offer/offer.repository';
import { MonetaryAmount } from '../../../../Shared/Domain/ValueObject/monetary-amount';
import { InstallmentNumber } from '../../../Domain/Shared/installment-number';

@CommandHandler(CreateOffer)
export class CreateOfferHandler implements ICommandHandler<CreateOffer> {
  constructor(
    @Inject('OfferRepository')
    private readonly offerRepository: OfferRepository,
    private eventBus: EventBus,
  ) {}

  async execute(command: CreateOffer) {
    const offer = Offer.createOffer(
      Uuid.fromString(command.id),
      Uuid.fromString(command.customerId),
      MonetaryAmount.fromDecimal(command.amount),
      new InstallmentNumber(command.installmentsNumber),
    );

    this.offerRepository.store(offer);

    this.eventBus.publish(offer.events.pop());
  }
}
