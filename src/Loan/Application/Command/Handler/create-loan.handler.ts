import { CommandHandler, EventBus, ICommandHandler } from '@nestjs/cqrs';
import { CreateLoan } from '../create-loan';
import { Uuid } from '../../../../Shared/Domain/ValueObject/uuid';
import { LoanRepository } from '../../../Domain/Loan/loan.repository';
import { Loan } from '../../../Domain/Loan/loan';
import { InstallmentNumber } from '../../../Domain/Shared/installment-number';
import { MonetaryAmount } from '../../../../Shared/Domain/ValueObject/monetary-amount';
import { LoanType } from '../../../Domain/Loan/ValueObject/loan-type';
import { Inject } from '@nestjs/common';
import { Apr } from '../../../Domain/Shared/apr';

@CommandHandler(CreateLoan)
export class CreateLoanHandler implements ICommandHandler<CreateLoan> {
  constructor(
    @Inject('LoanRepository')
    private readonly loanRepository: LoanRepository,
    private eventBus: EventBus,
  ) {}

  async execute(command: CreateLoan) {
    const customerId = Uuid.fromString(command.customerId);

    const loan =
      command.type === LoanType.BUSINESS_CREDIT
        ? Loan.createBusinessLoan(
            Uuid.fromString(command.id),
            MonetaryAmount.fromDecimal(command.totalAmount),
            customerId,
            new Apr(command.apr),
            new InstallmentNumber(command.numberOfInstallments),
          )
        : Loan.createConsumerLoan(
            Uuid.fromString(command.id),
            MonetaryAmount.fromDecimal(command.totalAmount),
            customerId,
            new Apr(command.apr),
            new InstallmentNumber(command.numberOfInstallments),
          );

    this.loanRepository.store(loan);

    this.eventBus.publish(loan.events.pop());
  }
}
