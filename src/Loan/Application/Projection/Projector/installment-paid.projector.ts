import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { InstallmentPaid } from '../../../Domain/CreditLine/Event/installment-paid';
import { CreditLineRepository } from '../ReadModel/credit-line.repository';
import { Inject } from '@nestjs/common';

@EventsHandler(InstallmentPaid)
export class InstallmentPaidProjector
  implements IEventHandler<InstallmentPaid>
{
  constructor(
    @Inject('ReadModelCreditLineRepository')
    private readonly creditLineRepository: CreditLineRepository,
  ) {}

  handle(event: InstallmentPaid) {
    const creditLimit = this.creditLineRepository.find(event.id.toString());

    creditLimit.totalPaidAmount =
      creditLimit.totalPaidAmount + event.amount.toDecimal();

    this.creditLineRepository.store(creditLimit);
  }
}
