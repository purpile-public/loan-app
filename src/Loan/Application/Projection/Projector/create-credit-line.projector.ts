import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { CreditLineRepository } from '../ReadModel/credit-line.repository';
import { CreditLineCreated } from '../../../Domain/CreditLine/Event/credit-line-created';
import { CreditLine } from '../ReadModel/credit-line';
import { Inject } from '@nestjs/common';

@EventsHandler(CreditLineCreated)
export class CreateCreditLineProjector
  implements IEventHandler<CreditLineCreated>
{
  constructor(
    @Inject('ReadModelCreditLineRepository')
    private readonly creditLineRepository: CreditLineRepository,
  ) {}

  handle(event: CreditLineCreated) {
    this.creditLineRepository.store(
      new CreditLine(
        event.id.toString(),
        event.loanId.toString(),
        event.totalCreditAmount.toDecimal(),
      ),
    );
  }
}
