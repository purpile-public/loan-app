import { CreditLine } from './credit-line';

export interface CreditLineRepository {
  store(creditLine: CreditLine): void;

  find(creditLineId: string): CreditLine;

  findAll(): CreditLine[];
}
