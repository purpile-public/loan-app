export class CreditLine {
  constructor(
    public readonly creditLineId: string,
    public readonly loanId: string,
    public totalPaidAmount: number,
  ) {}
}
