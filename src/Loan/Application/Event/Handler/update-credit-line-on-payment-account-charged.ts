import { CommandBus, EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { PaymentAccountCharged } from '../../../../PaymentAccount/Application/Event/payment-account-charged';
import { PayInstallment } from '../../Command/pay-installment';

@EventsHandler(PaymentAccountCharged)
export class UpdateCreditLineOnPaymentAccountCharged
  implements IEventHandler<PaymentAccountCharged>
{
  constructor(private readonly commandBus: CommandBus) {}

  handle(event: PaymentAccountCharged) {
    this.commandBus.execute(new PayInstallment(event.loanId, event.amount));
  }
}
