import { Module } from '@nestjs/common';
import { CqrsModule } from '@nestjs/cqrs';
import { CreateLoanHandler } from './Application/Command/Handler/create-loan.handler';
import { CreateCreditLineHandler } from './Application/Command/Handler/create-credit-line.handler';
import { UpdateCreditLineOnPaymentAccountCharged } from './Application/Event/Handler/update-credit-line-on-payment-account-charged';
import { LoanApi } from './UI/Http/loan-api';
import { InMemoryCreditLineRepository } from './Infrastructure/Repository/in-memory-credit-line.repository';
import { InMemoryLoanRepository } from './Infrastructure/Repository/in-memory-loan.repository';
import { RandomInMemoryAprCalculator } from './Infrastructure/Apr/random-in-memory-apr-calculator';
import { InMemoryCreditLineReadModelRepository } from './Infrastructure/Repository/in-memory-credit-line-read-model.repository';
import { CreateCreditLineProjector } from './Application/Projection/Projector/create-credit-line.projector';
import { InstallmentPaidProjector } from './Application/Projection/Projector/installment-paid.projector';
import { CustomerModule } from '../Customer/customer.module';
import { LoanCreatedManager } from './Application/ProcessManager/loan-created.manager';
import { OfferAcceptedManager } from './Application/ProcessManager/offer-accepted.manager';
import { OfferCreatedManager } from './Application/ProcessManager/offer-created.manager';
import { InMemoryOfferRepository } from './Infrastructure/Repository/in-memory-offer.repository';
import { MatrixRunner } from './Domain/Offer/Matrix/matrix-runner';
import { FraudMatrix } from './Domain/Offer/Matrix/Fraud/fraud.matrix';
import { AgeMatrix } from './Domain/Offer/Matrix/Age/age-matrix';
import { CreateOfferHandler } from './Application/Command/Handler/create-offer.handler';
import { AcceptOfferHandler } from './Application/Command/Handler/accept-offer.handler';
import { RunMatrixHandler } from './Application/Command/Handler/run-matrix.handler';
import { MatrixSuccessfulFinishedManager } from './Application/ProcessManager/matrix-successful-finished.manager';
import { ModuleCustomerProvider } from './Infrastructure/Provider/module-customer-provider';
import { XyzFraudChecker } from './Infrastructure/FraudProvider/xyz-fraud-checker';
import { XyzFraudClient } from './Infrastructure/FraudProvider/xyz-fraud-client';
import { CalculateAprHandler } from './Application/Command/Handler/calculate-apr.handler';
import { PayInstallmentHandler } from './Application/Command/Handler/pay-installment.handler';

export const ProcessManagers = [
  LoanCreatedManager,
  OfferAcceptedManager,
  OfferCreatedManager,
  MatrixSuccessfulFinishedManager,
];
export const EventHandlers = [UpdateCreditLineOnPaymentAccountCharged];
export const Projectors = [CreateCreditLineProjector, InstallmentPaidProjector];
export const CommandHandlers = [
  CreateLoanHandler,
  CreateCreditLineHandler,
  CreateOfferHandler,
  AcceptOfferHandler,
  RunMatrixHandler,
  CalculateAprHandler,
  PayInstallmentHandler,
];

@Module({
  imports: [CqrsModule, CustomerModule],
  providers: [
    {
      provide: 'MATRIX',
      useFactory: (...matrices) => {
        const matrix = new MatrixRunner();
        matrix.registerMatrices(matrices);
        return matrix;
      },
      inject: [FraudMatrix, AgeMatrix],
    },
    FraudMatrix,
    AgeMatrix,
    XyzFraudClient,
    ...EventHandlers,
    ...CommandHandlers,
    ...Projectors,
    ...ProcessManagers,
    {
      provide: 'FraudChecker',
      useClass: XyzFraudChecker,
    },
    {
      provide: 'CustomerProvider',
      useClass: ModuleCustomerProvider,
    },
    {
      provide: 'OfferRepository',
      useClass: InMemoryOfferRepository,
    },
    {
      provide: 'LoanRepository',
      useClass: InMemoryLoanRepository,
    },
    {
      provide: 'CreditLineRepository',
      useClass: InMemoryCreditLineRepository,
    },
    {
      provide: 'ReadModelCreditLineRepository',
      useClass: InMemoryCreditLineReadModelRepository,
    },
    {
      provide: 'AprCalculator',
      useClass: RandomInMemoryAprCalculator,
    },
  ],
  controllers: [LoanApi],
})
export class LoanModule {}
