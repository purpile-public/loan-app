import { Event } from './event';

export interface AggregateRoot {
  get events(): Array<Event>;
}
