import { Money } from 'ts-money';

export class MonetaryAmount {
  private static CURRENCY = 'USD';

  constructor(private _value: Money) {}

  public static fromInteger(value: number): MonetaryAmount {
    return new MonetaryAmount(Money.fromInteger(value, this.CURRENCY));
  }

  public static fromDecimal(value: number): MonetaryAmount {
    return new MonetaryAmount(Money.fromDecimal(value, this.CURRENCY));
  }

  toNumber(): number {
    return this._value.amount;
  }

  toDecimal(): number {
    return this._value.toDecimal();
  }

  public static default(): MonetaryAmount {
    return new MonetaryAmount(new Money(0, this.CURRENCY));
  }

  public lessThan(value: MonetaryAmount): boolean {
    return this._value.lessThan(value._value);
  }
}
