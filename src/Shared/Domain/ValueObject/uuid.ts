import { v4 } from 'uuid';

export class Uuid {
  private constructor(private _value: string) {
    //TODO validate
  }

  public static generate(): Uuid {
    return new Uuid(v4());
  }

  public static fromString(value: string): Uuid {
    return new Uuid(value);
  }

  public toString(): string {
    return this._value.toString();
  }
}
