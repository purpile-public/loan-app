import { Event } from './event';
import { AggregateRoot } from './aggregate-root';

export abstract class AggregateModel implements AggregateRoot {
  private _events: Event[] = [];

  get events(): Array<Event> {
    return this._events;
  }

  protected recordEvent(event: Event): void {
    this._events.push(event);
  }
}
