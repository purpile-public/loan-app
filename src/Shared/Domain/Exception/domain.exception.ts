export class DomainException extends Error {
  constructor(message: string) {
    super();
    this.name = 'DomainException';
    this.message = message;
  }
}
