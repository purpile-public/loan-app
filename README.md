## Loan application

An application has been written in `Modular Monolith architecture` using the `NestJS framework` with a Domain Driven Design approach.

## Problem

### Actors

* Customer - Our borrower, owner of the loan. Usually, the Customer agent must create an account for him based on some information. If account is created customer can apply for the loan by phone calling or visit credit office. Only 1 interaction what customer got with an application is `accept` offered by the unique link (not yet implemented)

* Customer Agent - Employee who works in a financial company. We don't have his context in our application (maybe in the future). This person creates customers in the system, generate offers, and is the first contact person for customer.

### Context

A loan app is created for easier applying for the loan. Application store customer information, which we can query more details for the customer, like affordability, fraud check, etc. Based on the credit score system generates a unique offer, and the customer can accept an offer. If the customer accepts the offer, the loan is created. Then every X day in the month (the company can define it). The system will charge all customers (Loan domain) an expected monthly instalment amount from their internal payment account.

### Assumptions
* The application doesn't have a real internal bank account IBAN etc. We just have a reference to our partner bank customer's account
* The Customer don't have their own portal for managing a loan
* The Customer can't overpay the loan

### Features for future
* Add time expiration for offer
* Extend APR calcualtor based on customer details
* Limit of amount in Loan
* Add validation for ValueObjects
* Collection boudnary
* Authentication for Agent by JWT
* Add swagger

## Architecture
### Used architectural design patterns and approaches such like:
* CQRS
* DDD
* Hexagonal
* Onion
* EventSourcing approach (in Loan boundary)
### C4 Diagram
TBD
### Events
In the presented application exists, two types of events:
* Domain events - Only boundary can subscribe on them, and only aggregate root can emit them by eventBus.
* Integration events - Application layer of given boundary is owner of events. Those events are used to integrate boundaries/services between domains.

## Boundaries
Customer - Module represent our "borrower". A person who takes a loan. `(Simple CRUD is nothing special domain logic for creating the customer)`

Loan - The boundary of representing loan management. `(DDD approach, EventSourcing (Projections, Read/Write model), Hexagonal, Onion)`

PaymentAccount - Internal customer account, where customers can pay loan instalments by bank transfer. `(DDD approach, Hexagonal, Onion)`

## Technical assumptions
* Account number is not real/valid IBAN format
* Implementation only in in-memory
* Domain layer should be agnostic, without external libs & dependencies. This application has some exceptions for libraries: Libs like `ts-money`, `uuid`, `validator`
* Create own `AggregateRoot` without using `AggreagteRoot` from NestJS CQRS Library (https://docs.nestjs.com/recipes/cqrs)

## Local development
Make file commands:
* `build` - build docker image
* `up` - run docker image in background by docker-compose
* `bash` - enter docker container
* `down` - kill running docker stack
