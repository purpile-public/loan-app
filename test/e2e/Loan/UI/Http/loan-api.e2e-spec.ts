import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';
import * as request from 'supertest';
import { AppModule } from '../../../../../src/app.module';
import { Uuid } from '../../../../../src/Shared/Domain/ValueObject/uuid';
import { LoanRepository } from '../../../../../src/Loan/Domain/Loan/loan.repository';
import { Loan } from '../../../../../src/Loan/Domain/Loan/loan';
import { MonetaryAmount } from '../../../../../src/Shared/Domain/ValueObject/monetary-amount';
import { Apr } from '../../../../../src/Loan/Domain/Shared/apr';
import { InstallmentNumber } from '../../../../../src/Loan/Domain/Shared/installment-number';
import { PaymentAccountRepository } from '../../../../../src/PaymentAccount/Domain/payment-account.repository';
import { CreditLineRepository } from '../../../../../src/Loan/Domain/CreditLine/credit-line.repository';
import { PaymentAccount } from '../../../../../src/PaymentAccount/Domain/payment-account';
import { AccountNumber } from '../../../../../src/PaymentAccount/Domain/ValueObject/account-number';
import { CreditLine } from '../../../../../src/Loan/Domain/CreditLine/credit-line';
import { OfferRepository } from '../../../../../src/Loan/Domain/Offer/offer.repository';
import { Offer } from '../../../../../src/Loan/Domain/Offer/offer';
import { RandomInMemoryAprCalculator } from '../../../../../src/Loan/Infrastructure/Apr/random-in-memory-apr-calculator';
import * as assert from 'assert';
import { InMemoryLoanRepository } from '../../../../../src/Loan/Infrastructure/Repository/in-memory-loan.repository';

describe('LoanApi (e2e)', () => {
  let app: INestApplication;
  let loanRepo: LoanRepository;
  let accountRepo: PaymentAccountRepository;
  let creditLineRepo: CreditLineRepository;
  let offerRepo: OfferRepository;

  beforeEach(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    app = moduleFixture.createNestApplication();
    loanRepo = moduleFixture.get('LoanRepository');
    accountRepo = moduleFixture.get('PaymentAccountRepository');
    creditLineRepo = moduleFixture.get('CreditLineRepository');
    offerRepo = moduleFixture.get('OfferRepository');
    await app.init();
  });

  it('will quote for loan', async () => {
    const response = await request(app.getHttpServer())
      .post('/api/loans/quote')
      .expect(201);

    const result = offerRepo.find(response.body.id);
    const stateOfObject = result.toSnapshot();

    expect(result).toBeDefined();
    expect(stateOfObject.offerState).toEqual('pending');
    expect(stateOfObject.numberOfInstallments).toEqual(10);
  });

  it('will accept offer and create loan', async () => {
    const uuid = Uuid.generate();

    const offer = Offer.createOffer(
      uuid,
      uuid,
      MonetaryAmount.fromDecimal(500),
      new InstallmentNumber(1),
    );

    offer.calculateApr(new RandomInMemoryAprCalculator());
    offer.events.pop();

    offerRepo.store(offer);

    await request(app.getHttpServer())
      .post(`/api/loans/offer/${uuid.toString()}/accept`)
      .expect(204);

    assert(loanRepo instanceof InMemoryLoanRepository);

    expect(loanRepo.getAll()).toHaveLength(1);
  });

  it('will charge payment for Loan', () => {
    const uuid = Uuid.generate();

    loanRepo.store(
      Loan.createConsumerLoan(
        uuid,
        MonetaryAmount.fromDecimal(500),
        uuid,
        new Apr(20),
        new InstallmentNumber(1),
      ),
    );

    const account = PaymentAccount.create(uuid, uuid, AccountNumber.generate());
    account.fund(MonetaryAmount.fromDecimal(500));
    accountRepo.store(account);

    creditLineRepo.store(
      CreditLine.create(
        Uuid.generate(),
        MonetaryAmount.fromDecimal(500),
        new InstallmentNumber(1),
        uuid,
      ),
    );

    return request(app.getHttpServer())
      .post(`/api/loans/${uuid.toString()}/charge-payment`)
      .send({ amount: '500' })
      .expect(204);
  });
});
