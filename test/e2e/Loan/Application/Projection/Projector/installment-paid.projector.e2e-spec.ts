import { INestApplication } from '@nestjs/common';
import { NestAppFactory } from '../../../../supports/nest-app.factory';
import { EventBus } from '@nestjs/cqrs';
import { InMemoryCreditLineReadModelRepository } from '../../../../../../src/Loan/Infrastructure/Repository/in-memory-credit-line-read-model.repository';
import { CreditLine } from '../../../../../../src/Loan/Application/Projection/ReadModel/credit-line';
import { Uuid } from '../../../../../../src/Shared/Domain/ValueObject/uuid';
import { InstallmentPaid } from '../../../../../../src/Loan/Domain/CreditLine/Event/installment-paid';
import { MonetaryAmount } from '../../../../../../src/Shared/Domain/ValueObject/monetary-amount';

describe('InstallmentPaidProjector (e2e)', () => {
  let app: INestApplication;
  let eventBus: EventBus;
  let repository: InMemoryCreditLineReadModelRepository;

  beforeEach(async () => {
    app = await NestAppFactory.createAppModule();
    eventBus = app.get(EventBus);
    repository = app.get('ReadModelCreditLineRepository');
    await app.init();
  });

  it('it will dispatch event and check if projector update a credit line', () => {
    const uuid = Uuid.generate();

    repository.store(new CreditLine(uuid.toString(), 'loanId', 0));

    eventBus.publish(
      new InstallmentPaid(uuid, MonetaryAmount.fromDecimal(1000)),
    );

    eventBus.publish(
      new InstallmentPaid(uuid, MonetaryAmount.fromDecimal(5000)),
    );

    const result = repository.find(uuid.toString());

    expect(result).toBeDefined();
    expect(result.totalPaidAmount).toEqual(6000);
  });
});
