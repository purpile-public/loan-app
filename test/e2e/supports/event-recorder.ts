import { Event } from '../../../src/Shared/Domain/event';

export class EventRecorder {
  static isEventRecorded(event: Event, events: Array<Event>): boolean {
    return (
      events.filter((e) => {
        return event === e.constructor;
      }).length > 0
    );
  }
}
