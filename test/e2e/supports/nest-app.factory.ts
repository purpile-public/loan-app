import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication, ValidationPipe } from '@nestjs/common';
import { TestingModuleBuilder } from '@nestjs/testing/testing-module.builder';
import { AppModule } from '../../../src/app.module';

export class NestAppFactory {
  static async createAppModule(): Promise<INestApplication> {
    let app: INestApplication = null;

    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    app = moduleFixture.createNestApplication();
    app.useGlobalPipes(
      new ValidationPipe({
        whitelist: true,
        transform: true,
      }),
    );

    return app;
  }
}
