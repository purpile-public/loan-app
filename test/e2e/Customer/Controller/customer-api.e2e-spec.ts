import { INestApplication } from '@nestjs/common';
import * as request from 'supertest';
import { CustomerRepository } from '../../../../src/Customer/Repository/customer.repository';
import { NestAppFactory } from '../../supports/nest-app.factory';
import { Customer } from '../../../../src/Customer/Model/customer';
import { Uuid } from '../../../../src/Shared/Domain/ValueObject/uuid';

describe('CustomerApi (e2e)', () => {
  let app: INestApplication;
  let customerRepository: CustomerRepository;

  beforeEach(async () => {
    app = await NestAppFactory.createAppModule();
    customerRepository = app.get('CustomerRepository');
    await app.init();
  });

  it('will create customer', () => {
    return request(app.getHttpServer())
      .post('/api/customers')
      .send({ firstName: 'John', lastName: 'Wick', age: 22 })
      .expect(201);
  });

  it('will update customer data', async () => {
    const customer = new Customer(
      Uuid.generate().toString(),
      'John',
      'Wick',
      22,
    );

    customerRepository.store(customer);

    await request(app.getHttpServer())
      .put(`/api/customers/${customer.id}`)
      .send({ firstName: 'Wick', lastName: 'John' })
      .expect(204);

    expect(customer.firstName).toEqual('Wick');
    expect(customer.lastName).toEqual('John');
  });

  it('will fetch customer details', () => {
    const customer = new Customer(
      Uuid.generate().toString(),
      'John',
      'Wick',
      22,
    );

    customerRepository.store(customer);

    return request(app.getHttpServer())
      .get(`/api/customers/${customer.id}`)
      .expect(200);
  });
});
