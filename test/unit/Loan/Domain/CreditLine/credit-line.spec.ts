import { CreditLine } from '../../../../../src/Loan/Domain/CreditLine/credit-line';
import { Uuid } from '../../../../../src/Shared/Domain/ValueObject/uuid';
import { MonetaryAmount } from '../../../../../src/Shared/Domain/ValueObject/monetary-amount';
import { InstallmentNumber } from '../../../../../src/Loan/Domain/Shared/installment-number';
import { EventRecorder } from '../../../../e2e/supports/event-recorder';
import { InstallmentPaid } from '../../../../../src/Loan/Domain/CreditLine/Event/installment-paid';
import { CreditLineCreated } from '../../../../../src/Loan/Domain/CreditLine/Event/credit-line-created';

describe('CreditLine', () => {
  it('should create credit line', () => {
    expect(
      CreditLine.create(
        Uuid.generate(),
        MonetaryAmount.fromDecimal(0),
        new InstallmentNumber(0),
        Uuid.generate(),
      ),
    ).toBeDefined();
  });

  it('should pay installment', () => {
    const creditLine = CreditLine.create(
      Uuid.generate(),
      MonetaryAmount.fromDecimal(2400),
      new InstallmentNumber(24),
      Uuid.generate(),
    );

    creditLine.payInstallment(MonetaryAmount.fromDecimal(100));

    expect(
      EventRecorder.isEventRecorded(InstallmentPaid, creditLine.events),
    ).toBeTruthy();
  });

  it('create from event history', () => {
    const id = Uuid.generate();

    const events = [
      new CreditLineCreated(
        id,
        MonetaryAmount.fromDecimal(100),
        new InstallmentNumber(10),
        Uuid.generate(),
      ),
      new InstallmentPaid(id, MonetaryAmount.fromDecimal(200)),
      new InstallmentPaid(id, MonetaryAmount.fromDecimal(200)),
    ];

    expect(CreditLine.createFromHistory(events)).toBeDefined();
  });
});
