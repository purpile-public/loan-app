import { LoanTotalAmountCalculator } from '../../../../../src/Loan/Domain/Loan/loan-total-amount-calculator';
import { Apr } from '../../../../../src/Loan/Domain/Shared/apr';
import { MonetaryAmount } from '../../../../../src/Shared/Domain/ValueObject/monetary-amount';

describe('LoanTotalAmountCalculator', () => {
  it('should create business Loan', () => {
    const apr = new Apr(10);
    const money = MonetaryAmount.fromDecimal(10000);

    const calc = LoanTotalAmountCalculator.calculate(apr, money);

    expect(calc.toNumber()).toEqual(1100000);
    expect(calc.toDecimal()).toEqual(11000);
  });
});
