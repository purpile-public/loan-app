import { Uuid } from '../../../../../src/Shared/Domain/ValueObject/uuid';
import { MonetaryAmount } from '../../../../../src/Shared/Domain/ValueObject/monetary-amount';
import { Loan } from '../../../../../src/Loan/Domain/Loan/loan';
import { Apr } from '../../../../../src/Loan/Domain/Shared/apr';
import { InstallmentNumber } from '../../../../../src/Loan/Domain/Shared/installment-number';

describe('Loan', () => {
  it('should create business Loan', () => {
    expect(
      Loan.createBusinessLoan(
        Uuid.generate(),
        MonetaryAmount.default(),
        Uuid.generate(),
        new Apr(20.15),
        new InstallmentNumber(10),
      ),
    ).toBeDefined();
  });
  it('should create consumer Loan', () => {
    expect(
      Loan.createConsumerLoan(
        Uuid.generate(),
        MonetaryAmount.default(),
        Uuid.generate(),
        new Apr(20.43),
        new InstallmentNumber(0),
      ),
    ).toBeDefined();
  });
});
