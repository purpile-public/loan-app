import { Uuid } from '../../../../../src/Shared/Domain/ValueObject/uuid';
import { MonetaryAmount } from '../../../../../src/Shared/Domain/ValueObject/monetary-amount';
import { InstallmentNumber } from '../../../../../src/Loan/Domain/Shared/installment-number';
import { CreditLine } from '../../../../../src/Loan/Domain/CreditLine/credit-line';
import { InMemoryCreditLineRepository } from '../../../../../src/Loan/Infrastructure/Repository/in-memory-credit-line.repository';

describe('InMemoryCreditLineRepository', () => {
  it('should add events to existed credit line aggregate root', () => {
    const repository = new InMemoryCreditLineRepository();

    const creditLine = CreditLine.create(
      Uuid.generate(),
      MonetaryAmount.fromDecimal(10000),
      new InstallmentNumber(10),
      Uuid.generate(),
    );

    repository.store(creditLine);

    creditLine.payInstallment(MonetaryAmount.fromDecimal(1000));

    repository.store(creditLine);

    const all = repository.getAll();

    expect(all).toHaveLength(2);
  });
});
