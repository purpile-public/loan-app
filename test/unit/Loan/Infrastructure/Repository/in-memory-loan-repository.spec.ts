import { Uuid } from '../../../../../src/Shared/Domain/ValueObject/uuid';
import { MonetaryAmount } from '../../../../../src/Shared/Domain/ValueObject/monetary-amount';
import { InMemoryLoanRepository } from '../../../../../src/Loan/Infrastructure/Repository/in-memory-loan.repository';
import { Apr } from '../../../../../src/Loan/Domain/Shared/apr';
import { InstallmentNumber } from '../../../../../src/Loan/Domain/Shared/installment-number';
import { Loan } from '../../../../../src/Loan/Domain/Loan/loan';
import { EventRecorder } from '../../../../e2e/supports/event-recorder';
import { LoanFinished } from '../../../../../src/Loan/Domain/Loan/Event/loan-finished';

describe('InMemoryLoanRepository', () => {
  it('should add events to existed Loan aggregate root', () => {
    const repository = new InMemoryLoanRepository();

    const loan = Loan.createBusinessLoan(
      Uuid.generate(),
      MonetaryAmount.fromDecimal(0),
      Uuid.generate(),
      new Apr(20.15),
      new InstallmentNumber(10),
    );

    repository.store(loan);

    loan.finished(new Date());

    repository.store(loan);

    const all = repository.getAll();

    expect(
      EventRecorder.isEventRecorded(LoanFinished, all[0]['events']),
    ).toBeTruthy();
  });
});
