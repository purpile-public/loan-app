import { CreditLine } from '../../../../../src/Loan/Application/Projection/ReadModel/credit-line';
import { InMemoryCreditLineReadModelRepository } from '../../../../../src/Loan/Infrastructure/Repository/in-memory-credit-line-read-model.repository';

describe('InMemoryCreditLineReadModelRepository', () => {
  it('should find read model after store by creditLineId', () => {
    const repository = new InMemoryCreditLineReadModelRepository();

    const creditLine = new CreditLine('uuid', 'uuid', 1000);

    repository.store(creditLine);

    const result = repository.find(creditLine.creditLineId);

    expect(result.totalPaidAmount).toEqual(1000);
  });

  it('should overwrite value if exists', () => {
    const repository = new InMemoryCreditLineReadModelRepository();

    repository.store(new CreditLine('uuid', 'uuid', 1000));
    repository.store(new CreditLine('uuid', 'uuid', 2000));
    repository.store(new CreditLine('uuid', 'uuid', 5000));

    const result = repository.find('uuid');

    expect(result.totalPaidAmount).toEqual(5000);
  });
});
